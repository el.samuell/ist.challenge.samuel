package ist.challenge.samuel.user.controller.update;

import com.fasterxml.jackson.databind.ObjectMapper;
import ist.challenge.samuel.constant.Routes;
import ist.challenge.samuel.user.controller.registrasi.UserRegisterRequestDto;
import ist.challenge.samuel.user.usecase.registrasi.UserRegister;
import ist.challenge.samuel.user.usecase.update.UserUpdate;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(UserUpdateController.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@MockBean(UserUpdate.class)
class UserUpdateControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @SneakyThrows
    void givenRequest_whenUpdate_shouldReturnOk() {

        UserUpdateRequestDto request = new UserUpdateRequestDto();
        Long id = 1L;
        request.setPassword("dummy");
        request.setUsername("dummy");
        request.setId(id);
        String requestBody = new ObjectMapper().writeValueAsString(request);
        mockMvc.perform(MockMvcRequestBuilders.put(Routes.USER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .param("id", String.valueOf(id))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(201));
    }
}