package ist.challenge.samuel.user.controller.list_user;

import ist.challenge.samuel.constant.Routes;
import ist.challenge.samuel.user.usecase.list_user.UserList;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;


@WebMvcTest(UserListController.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@MockBean(UserList.class)
class UserListControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    @SneakyThrows
    void givenRequest_whenGetUserList_shouldReturnOk() {

        mockMvc.perform(MockMvcRequestBuilders.get(Routes.USER))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

}