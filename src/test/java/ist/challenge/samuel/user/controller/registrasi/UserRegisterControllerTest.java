package ist.challenge.samuel.user.controller.registrasi;

import com.fasterxml.jackson.databind.ObjectMapper;
import ist.challenge.samuel.constant.Routes;
import ist.challenge.samuel.user.controller.login.UserLoginRequestDto;
import ist.challenge.samuel.user.usecase.registrasi.UserRegister;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(UserRegisterController.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@MockBean(UserRegister.class)
class UserRegisterControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @SneakyThrows
    void givenRequest_whenRegister_shouldReturnOk() {

        UserRegisterRequestDto request = new UserRegisterRequestDto("dummy", "dummy");
        String requestBody = new ObjectMapper().writeValueAsString(request);
        mockMvc.perform(MockMvcRequestBuilders.post(Routes.REGISTER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(201));
    }
}