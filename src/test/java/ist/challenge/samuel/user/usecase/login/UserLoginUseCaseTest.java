package ist.challenge.samuel.user.usecase.login;

import ist.challenge.samuel.exception.BadRequestException;
import ist.challenge.samuel.user.controller.login.UserLoginRequestDto;
import ist.challenge.samuel.user.entity.User;
import ist.challenge.samuel.user.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


class UserLoginUseCaseTest {

    @Mock
    private UserLoginRequest request;

    @Mock
    private UserLoginPresenter presenter;

    @InjectMocks
    private UserLoginUseCase useCase;

    @Mock
    private User user;

    @Mock
    private UserRepository userRepository;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void givenNullUsername_whenLogin_shouldThrowException() {
        request = new UserLoginRequestDto(null,"dummy");
        Executable task = () -> useCase.login(request, presenter);

        Exception e = assertThrows(BadRequestException.class,task);

        assertThat(e.getMessage()).isEqualTo("Username dan / atau password kosong");
    }

    @Test
    void givenNullPassword_whenLogin_shouldThrowException() {
        request = new UserLoginRequestDto("dummy",null);
        Executable task = () -> useCase.login(request, presenter);

        Exception e = assertThrows(BadRequestException.class,task);

        assertThat(e.getMessage()).isEqualTo("Username dan / atau password kosong");
    }


    @Test
    void givenEmptyUsername_whenLogin_shouldThrowException() {
        request = new UserLoginRequestDto("","dummy");
        Executable task = () -> useCase.login(request, presenter);

        Exception e = assertThrows(BadRequestException.class,task);

        assertThat(e.getMessage()).isEqualTo("Username dan / atau password kosong");
    }

    @Test
    void givenEmptyPassword_whenLogin_shouldThrowException() {
        request = new UserLoginRequestDto("dummy","");
        Executable task = () -> useCase.login(request, presenter);

        Exception e = assertThrows(BadRequestException.class, task);

        assertThat(e.getMessage()).isEqualTo("Username dan / atau password kosong");
    }

    @Test
    void givenWrongPassword_whenLogin_shouldThrowException(){
        request = new UserLoginRequestDto("dummy", "wrong");
        stubUser();
        Executable task = () -> useCase.login(request, presenter);

        Exception e = assertThrows(BadRequestException.class, task);

        assertThat(e.getMessage()).isEqualTo("Password tidak cocok");
    }

    @Test
    void givenCorrectUsernameAndPassword_whenLogin_shouldCallPresenterWithCorrectResponse(){
        prepareAndExecute();
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(presenter).present(captor.capture());
        String actual = captor.getValue();
        assertThat(actual).isNotNull();
    }

    private void prepareAndExecute() {
        stubUser();
        request = new UserLoginRequestDto("dummy", "dummy");
        useCase.login(request, presenter);
    }

    private void stubUser() {
        when(user.getId()).thenReturn(1L);
        when(user.getUsername()).thenReturn("dummy");
        when(user.getPassword()).thenReturn("dummy");
        when(userRepository.findByUsername(any())).thenReturn(Optional.ofNullable(user));
    }



}