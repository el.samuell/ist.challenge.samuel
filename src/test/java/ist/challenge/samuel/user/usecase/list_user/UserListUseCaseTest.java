package ist.challenge.samuel.user.usecase.list_user;

import ist.challenge.samuel.user.entity.User;
import ist.challenge.samuel.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class UserListUseCaseTest {

    @InjectMocks
    private UserListUseCase useCase;

    @Mock
    UserListPresenter presenter;

    @Mock
    private User user;

    @Mock
    private UserRepository userRepository;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void returnList_whenCallUserList() {
        stubUser();
        useCase.get(presenter);
        ArgumentCaptor<List<User>> captor = ArgumentCaptor.forClass(List.class);
        verify(presenter).present(captor.capture());
        List<User> actual = captor.getValue();
        assertThat(actual.get(0)).isNotNull();
    }

    private void stubUser() {

        List<User> list = mock(List.class);
        when(list.get(0)).thenReturn(new User(1L, "dummy_1", "dummy"));
        when(list.get(1)).thenReturn(new User(2L, "dummy_2", "dummy"));
        when(list.get(2)).thenReturn(new User(3L, "dummy_3", "dummy"));
        when(userRepository.findAll()).thenReturn(list);
    }
}