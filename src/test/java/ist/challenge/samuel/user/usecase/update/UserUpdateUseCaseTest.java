package ist.challenge.samuel.user.usecase.update;

import ist.challenge.samuel.exception.BadRequestException;
import ist.challenge.samuel.exception.ConflictException;
import ist.challenge.samuel.user.controller.update.UserUpdateRequestDto;
import ist.challenge.samuel.user.entity.User;
import ist.challenge.samuel.user.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserUpdateUseCaseTest {

    @Mock
    private UserUpdateRequest request;

    @InjectMocks
    private UserUpdateUseCase useCase;

    @Mock
    private User user;

    @Mock
    private UserRepository userRepository;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void givenNotExistId_whenUpdate_shouldThrowException() {
        request = new UserUpdateRequestDto("dummy", "dummy", 2L);
        stubUser();
        Executable task = () -> useCase.update(request);

        Exception e = assertThrows(BadRequestException.class, task);

        assertThat(e.getMessage()).isEqualTo("User tidak ditemukan");
    }

    @Test
    void givenExistUsername_whenUpdate_shouldThrowException() {
        request = new UserUpdateRequestDto("dummy", "another", 1L);
        stubUser();
        when(userRepository.findByUsername(request.getUsername())).thenReturn(Optional.ofNullable(user));
        Executable task = () -> useCase.update(request);

        Exception e = assertThrows(ConflictException.class, task);

        assertThat(e.getMessage()).isEqualTo("Username sudah terpakai");
    }

    @Test
    void givenExistingPassword_whenUpdate_shouldThrowException() {
        request = new UserUpdateRequestDto("dummy", "dummy", 1L);
        stubUser();
        Executable task = () -> useCase.update(request);

        Exception e = assertThrows(BadRequestException.class, task);

        assertThat(e.getMessage()).isEqualTo("Password tidak boleh sama dengan password sebelumnya");
    }

    @Test
    void givenNotExistUsernameAndNotExistingPassword_whenUpdate_shouldCorrect(){
        stubUser();
        request = new UserUpdateRequestDto("another", "another", 1L);
        useCase.update(request);
        verify(userRepository).save(request.toEntity(user));
    }

    private void stubUser() {
        when(user.getId()).thenReturn(1L);
        when(user.getUsername()).thenReturn("dummy");
        when(user.getPassword()).thenReturn("dummy");
        when(userRepository.findById(1L)).thenReturn(Optional.ofNullable(user));
    }

}