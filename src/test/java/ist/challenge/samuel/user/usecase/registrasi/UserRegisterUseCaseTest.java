package ist.challenge.samuel.user.usecase.registrasi;

import ist.challenge.samuel.exception.ConflictException;
import ist.challenge.samuel.user.controller.registrasi.UserRegisterRequestDto;
import ist.challenge.samuel.user.entity.User;
import ist.challenge.samuel.user.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserRegisterUseCaseTest {

    @Mock
    private UserRegisterRequest request;

    @InjectMocks
    private UserRegisterUseCase useCase;

    @Mock
    private User user;

    @Mock
    private UserRepository userRepository;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void givenExistUsername_whenRegister_shouldThrowException() {
        request = new UserRegisterRequestDto("dummy", "dummy");
        stubUser();
        Executable task = () -> useCase.register(request);

        Exception e = assertThrows(ConflictException.class, task);

        assertThat(e.getMessage()).isEqualTo("Username sudah terpakai");
    }


    @Test
    void givenNotExistUsername_whenRegister_shouldReturnCorrect(){
        stubUser();
        request = new UserRegisterRequestDto("another", "dummy");
        useCase.register(request);
        verify(userRepository).save(request.toEntity());
    }

    private void stubUser() {
        when(user.getId()).thenReturn(1L);
        when(user.getUsername()).thenReturn("dummy");
        when(user.getPassword()).thenReturn("dummy");
        when(userRepository.findByUsername("dummy")).thenReturn(Optional.ofNullable(user));
    }

}