package ist.challenge.samuel.constant;

public class Routes {
    public static final String USER = "/users";
    public static final String REGISTER = USER + "/register";
    public static final String LOGIN = USER + "/login";
}
