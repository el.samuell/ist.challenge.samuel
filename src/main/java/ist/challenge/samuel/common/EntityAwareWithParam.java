package ist.challenge.samuel.common;

public interface EntityAwareWithParam<T> {
    T toEntity(T data);
}
