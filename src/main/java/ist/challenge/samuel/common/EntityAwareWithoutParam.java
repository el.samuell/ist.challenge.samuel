package ist.challenge.samuel.common;

public interface EntityAwareWithoutParam<T> {
    T toEntity();
}
