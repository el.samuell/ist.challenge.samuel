package ist.challenge.samuel.config;


import ist.challenge.samuel.exception.BadRequestException;
import ist.challenge.samuel.exception.ConflictException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class ExceptionTranslator {

    @ExceptionHandler(value = {RuntimeException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String processGenericException(java.lang.Exception e, HttpServletRequest request) {
        return e.getMessage();
    }

    @ExceptionHandler(value = {ConflictException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public String processConflictException(Exception e, HttpServletRequest request) {
        return e.getMessage();
    }

    @ExceptionHandler(value = {BadRequestException.class, ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String processBadRequestException(Exception e, HttpServletRequest request) {
        return e.getMessage();
    }

}
