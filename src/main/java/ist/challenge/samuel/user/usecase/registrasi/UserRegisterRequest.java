package ist.challenge.samuel.user.usecase.registrasi;

import ist.challenge.samuel.common.EntityAwareWithoutParam;
import ist.challenge.samuel.user.entity.User;

public interface UserRegisterRequest extends EntityAwareWithoutParam<User> {
    String getUsername();
}
