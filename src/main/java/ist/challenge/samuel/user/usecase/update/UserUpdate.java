package ist.challenge.samuel.user.usecase.update;

public interface UserUpdate {
    void update(UserUpdateRequest request);
}
