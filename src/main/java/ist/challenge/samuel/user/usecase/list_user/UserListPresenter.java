package ist.challenge.samuel.user.usecase.list_user;

import ist.challenge.samuel.user.entity.User;

import java.util.List;

public interface UserListPresenter {
    void present(List<User> users);

}
