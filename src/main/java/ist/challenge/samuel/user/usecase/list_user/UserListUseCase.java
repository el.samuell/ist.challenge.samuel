package ist.challenge.samuel.user.usecase.list_user;

import ist.challenge.samuel.user.entity.User;
import ist.challenge.samuel.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserListUseCase implements UserList {

    private final UserRepository userRepository;

    @Override
    public void get(UserListPresenter presenter) {
        List<User> users = userRepository.findAll();
        presenter.present(users);
    }
}
