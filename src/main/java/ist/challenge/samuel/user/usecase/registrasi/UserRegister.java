package ist.challenge.samuel.user.usecase.registrasi;

public interface UserRegister {
    void register(UserRegisterRequest request);
}
