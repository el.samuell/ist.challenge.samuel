package ist.challenge.samuel.user.usecase.update;

import ist.challenge.samuel.common.EntityAwareWithParam;
import ist.challenge.samuel.user.entity.User;

public interface UserUpdateRequest extends EntityAwareWithParam<User> {
    String getUsername();

    String getPassword();

    Long getId();
}
