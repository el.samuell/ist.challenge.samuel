package ist.challenge.samuel.user.usecase.login;

public interface UserLoginRequest {
    String getUsername();

    String getPassword();
}
