package ist.challenge.samuel.user.usecase.login;

public interface UserLoginPresenter {
    void present(String message);
}
