package ist.challenge.samuel.user.usecase.login;

public interface UserLogin {
    void login(UserLoginRequest request, UserLoginPresenter presenter);
}
