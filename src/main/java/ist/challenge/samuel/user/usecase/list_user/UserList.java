package ist.challenge.samuel.user.usecase.list_user;

public interface UserList {
    void get(UserListPresenter presenter);
}
