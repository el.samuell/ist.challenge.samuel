package ist.challenge.samuel.user.usecase.update;

import ist.challenge.samuel.exception.BadRequestException;
import ist.challenge.samuel.exception.ConflictException;
import ist.challenge.samuel.user.entity.User;
import ist.challenge.samuel.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserUpdateUseCase implements UserUpdate {

    private final UserRepository userRepository;

    @Override
    public void update(UserUpdateRequest request) {
        User user = userRepository.findById(request.getId())
                .orElseThrow(() -> {
                    throw new BadRequestException("User tidak ditemukan");
                });
        validateExistUsername(request.getUsername());
        validateExistingPassword(user.getPassword(), request.getPassword());
        userRepository.save(request.toEntity(user));
    }

    private void validateExistingPassword(String existingPassword, String password) {
        if (existingPassword.equals(password)) {
            throw new BadRequestException("Password tidak boleh sama dengan password sebelumnya");
        }
    }

    private void validateExistUsername(String username) {
        userRepository.findByUsername(username).ifPresent(user -> {
            throw new ConflictException("Username sudah terpakai");
        });

    }
}
