package ist.challenge.samuel.user.usecase.registrasi;

import ist.challenge.samuel.exception.ConflictException;
import ist.challenge.samuel.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserRegisterUseCase implements UserRegister {

    private final UserRepository userRepository;

    @Override
    public void register(UserRegisterRequest request) {
        userRepository.findByUsername(request.getUsername()).
                ifPresent(user -> {
                    throw new ConflictException("Username sudah terpakai");
                });
        userRepository.save(request.toEntity());
    }
}
