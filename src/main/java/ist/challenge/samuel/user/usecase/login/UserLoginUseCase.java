package ist.challenge.samuel.user.usecase.login;

import ist.challenge.samuel.exception.BadRequestException;
import ist.challenge.samuel.user.entity.User;
import ist.challenge.samuel.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserLoginUseCase implements UserLogin {

    private final UserRepository userRepository;

    @Override
    public void login(UserLoginRequest request, UserLoginPresenter presenter) {

        validateRequest(request);

        User user = userRepository.findByUsername(request.getUsername())
                .orElseThrow(() -> {
                    throw new BadRequestException("User tidak ditemukan");
                });

        if (!request.getPassword().equals(user.getPassword())) {
            throw new BadRequestException("Password tidak cocok");
        }

        presenter.present("Sukses Login");
    }

    private void validateRequest(UserLoginRequest request) {

        if(request.getUsername()==null || request.getPassword()==null){
            throw new BadRequestException("Username dan / atau password kosong");
        }

        if(request.getUsername().isEmpty() || request.getPassword().isEmpty()){
            throw new BadRequestException("Username dan / atau password kosong");
        }
    }
}
