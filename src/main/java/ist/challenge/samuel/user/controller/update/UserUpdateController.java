package ist.challenge.samuel.user.controller.update;

import ist.challenge.samuel.constant.Routes;
import ist.challenge.samuel.user.usecase.update.UserUpdate;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class UserUpdateController {

    private final UserUpdate userUpdate;

    @PutMapping(Routes.USER)
    @ResponseStatus(HttpStatus.CREATED)
    void update(@RequestBody UserUpdateRequestDto request, @RequestParam("id") Long id) {
        request.setId(id);
        userUpdate.update(request);
    }

}
