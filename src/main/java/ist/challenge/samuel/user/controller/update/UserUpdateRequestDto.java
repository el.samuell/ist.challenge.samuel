package ist.challenge.samuel.user.controller.update;

import ist.challenge.samuel.user.entity.User;
import ist.challenge.samuel.user.usecase.update.UserUpdateRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateRequestDto implements UserUpdateRequest {
    String username;
    String password;
    Long id;

    @Override
    public User toEntity(User data) {
        return User.builder()
                .id(data.getId())
                .username(username)
                .password(password)
                .build();
    }
}
