package ist.challenge.samuel.user.controller.registrasi;

import ist.challenge.samuel.constant.Routes;
import ist.challenge.samuel.user.usecase.registrasi.UserRegister;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserRegisterController {

    private final UserRegister userRegister;

    @PostMapping(Routes.REGISTER)
    @ResponseStatus(HttpStatus.CREATED)
    void register(@RequestBody UserRegisterRequestDto request) {
        userRegister.register(request);
    }

}
