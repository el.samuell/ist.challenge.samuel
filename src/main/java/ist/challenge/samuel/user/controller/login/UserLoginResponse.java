package ist.challenge.samuel.user.controller.login;

import ist.challenge.samuel.user.usecase.login.UserLoginPresenter;

public class UserLoginResponse implements UserLoginPresenter {
    String message;

    @Override
    public void present(String message) {
        this.message = message;
    }

    String get() {
        return message;
    }
}
