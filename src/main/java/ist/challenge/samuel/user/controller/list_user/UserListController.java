package ist.challenge.samuel.user.controller.list_user;

import ist.challenge.samuel.constant.Routes;
import ist.challenge.samuel.user.entity.User;
import ist.challenge.samuel.user.usecase.list_user.UserList;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserListController {

    private final UserList userList;

    @GetMapping(Routes.USER)
    @ResponseStatus(HttpStatus.OK)
    List<User> list() {
        UserListResponse response = new UserListResponse();
        userList.get(response);
        return response.get();
    }

}
