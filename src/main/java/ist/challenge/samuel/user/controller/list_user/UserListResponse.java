package ist.challenge.samuel.user.controller.list_user;

import ist.challenge.samuel.user.entity.User;
import ist.challenge.samuel.user.usecase.list_user.UserListPresenter;

import java.util.List;

public class UserListResponse implements UserListPresenter {

    List<User> userList;

    @Override
    public void present(List<User> users) {
        userList = List.copyOf(users);
    }

    List<User> get() {
        return userList;
    }
}
