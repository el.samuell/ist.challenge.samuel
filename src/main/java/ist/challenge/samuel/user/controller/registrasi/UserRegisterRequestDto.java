package ist.challenge.samuel.user.controller.registrasi;

import ist.challenge.samuel.user.entity.User;
import ist.challenge.samuel.user.usecase.registrasi.UserRegisterRequest;
import lombok.Value;

@Value
public class UserRegisterRequestDto implements UserRegisterRequest {
    String username;
    String password;

    @Override
    public User toEntity() {
        return User.builder()
                .username(username)
                .password(password)
                .build();
    }
}
