package ist.challenge.samuel.user.controller.login;

import ist.challenge.samuel.constant.Routes;
import ist.challenge.samuel.user.usecase.login.UserLogin;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserLoginController {

    private final UserLogin userLogin;

    @PostMapping(Routes.LOGIN)
    @ResponseStatus(HttpStatus.OK)
    String login(@RequestBody UserLoginRequestDto request) {
        UserLoginResponse response = new UserLoginResponse();
        userLogin.login(request, response);
        return response.get();
    }

}
