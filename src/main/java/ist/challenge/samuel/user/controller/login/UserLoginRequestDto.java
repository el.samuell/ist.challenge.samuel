package ist.challenge.samuel.user.controller.login;

import ist.challenge.samuel.user.usecase.login.UserLoginRequest;
import lombok.Value;

@Value
public class UserLoginRequestDto implements UserLoginRequest {
    String username;
    String password;
}
